//
// Dedicated Resource
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <cstdint>      // int32_t
#include <stdexcept>
#include <QtCore>

#include <dedicatedresource.h>

extern "C" {
    #include <pv_display_backend_helper.h>
};

// ============================================================================
// Implementation
// ============================================================================

DedicatedResource::DedicatedResource(const uuid_t uuid, const domid_t domid) : m_uuid(uuid), m_domid(domid), m_next_pv_port(2000), m_pv_backend_consumer(nullptr) {
    init();
}

void DedicatedResource::init() {
    int32_t rc = 0;

    destroy();

    rc = create_pv_display_consumer(&m_pv_backend_consumer, m_domid, m_next_pv_port, (void*) this);

    if (rc || !m_pv_backend_consumer) {
        throw std::runtime_error("Failed to create pv backend.");
    }

    m_pv_backend_consumer->set_driver_data(m_pv_backend_consumer, this);

    // Register a display advertised list request handler.
    m_pv_backend_consumer->register_display_advertised_list_request_handler(
        m_pv_backend_consumer, &DedicatedResource::pv_advertise_list_request);

    // Register a Fatal Error handler
    m_pv_backend_consumer->register_fatal_error_handler(
        m_pv_backend_consumer, &DedicatedResource::pv_driver_fatal_consumer_error);

    m_pv_backend_consumer->register_control_connection_handler(
        m_pv_backend_consumer, &DedicatedResource::new_control_channel_connection);

    rc = m_pv_backend_consumer->start_server(m_pv_backend_consumer);

    if (rc) {
        throw std::runtime_error("Failed to create control server for nxus support");
    }
}

void DedicatedResource::destroy() {
    if (m_pv_backend_consumer) {
        destroy_pv_display_consumer(m_pv_backend_consumer);
        m_pv_backend_consumer = nullptr;
    }
}

DedicatedResource::~DedicatedResource() {
    destroy();
}

void DedicatedResource::pv_advertise_list_request(struct pv_display_consumer *consumer,
                                                  struct dh_display_advertised_list *advertised_list) {
    DedicatedResource *self = static_cast<DedicatedResource *>(consumer->get_driver_data(consumer));

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    uint32_t size = advertised_list->num_displays;
    struct dh_display_info * display_info = advertised_list->displays;

    for (uint32_t i = 0; i < size; i++) {
        if (display_info[i].key & DISCRETE_MASK) {
            QMetaObject::invokeMethod(self, "add_display", Qt::QueuedConnection,
                                      Q_ARG(uint32_t, display_info[i].key),
                                      Q_ARG(uint32_t, display_info[i].x),
                                      Q_ARG(uint32_t, display_info[i].y),
                                      Q_ARG(uint32_t, display_info[i].width),
                                      Q_ARG(uint32_t, display_info[i].height));
        } else {
            qDebug() << "Display ignored :" << display_info[i].key;
        }
    }

    QMetaObject::invokeMethod(self, "remove_old_displays", Qt::QueuedConnection);

    QMetaObject::invokeMethod(self, "displaysChanged", Qt::QueuedConnection);
}

void DedicatedResource::pv_driver_fatal_consumer_error(struct pv_display_consumer *consumer) {
    DedicatedResource *self = static_cast<DedicatedResource *>(consumer->get_driver_data(consumer));

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self, "handle_error", Qt::QueuedConnection);
}

void
DedicatedResource::new_control_channel_connection(void *opaque,
                                                  struct libivc_client *client)
{
    (void) client;
    DedicatedResource *self = static_cast<DedicatedResource *>(opaque);

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self,
                              "finish_control_connection",
                              Qt::QueuedConnection,
                              Q_ARG(void *, (void *)client));
}

void
DedicatedResource::finish_control_connection(void *cli)
{
    struct libivc_client *client = (struct libivc_client *)cli;

    if (m_pv_backend_consumer) {
        m_pv_backend_consumer->finish_control_connection(m_pv_backend_consumer, client);
    }
}

void DedicatedResource::add_display(uint32_t key, uint32_t x, uint32_t y, uint32_t width, uint32_t height) {
    m_newkeys << key;

    if (m_display_info_internal.contains(key)) {
        qDebug() << "Display changed :";
        qDebug() << "key    :" << key;
        qDebug() << "x      :" << x;
        qDebug() << "y      :" << y;
        qDebug() << "width  :" << width;
        qDebug() << "height :" << height;
    } else {
        qDebug() << "Display added :";
        qDebug() << "key    :" << key;
        qDebug() << "x      :" << x;
        qDebug() << "y      :" << y;
        qDebug() << "width  :" << width;
        qDebug() << "height :" << height;
    }

    dh_display_info info = { key, x, y, width, height, 0 };
    m_display_info_internal.insert(key, info);
}

void DedicatedResource::remove_old_displays() {
    for (auto& key : m_display_info_internal.keys().toSet() - m_newkeys) {
        qDebug() << "Display removed :" << key;
        m_display_info_internal.remove(key);
    }
}

void DedicatedResource::displaysChanged() {
    uint32_t i = 0;
    qDebug() << "New Displays           :" << m_display_info_internal.keys();
    m_newkeys.clear();
    emit displays_changed(m_uuid, m_display_info_internal.values(), m_display_info_internal.size());
    std::unique_ptr<dh_display_info[]> displays = std::move(std::make_unique<dh_display_info[]>(m_display_info_internal.values().size()));
    for (auto &display : m_display_info_internal.values())
    {
        displays[i].key = display.key;
        displays[i].x = display.x;
        displays[i].y = display.y;
        displays[i].width = display.width;
        displays[i].height = display.height;
        i++;
    }
    m_pv_backend_consumer->display_list(m_pv_backend_consumer, displays.get(), m_display_info_internal.size());
}

void DedicatedResource::handle_error() {
    qInfo() << "Fatal error occured within the PV Backend.";
    destroy();
    init();
}
