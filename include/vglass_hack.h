#ifndef VGLASS_HACK_H
#define VGLASS_HACK_H

//
// VGlass Hack
//
// Copyright (C) 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <cstdint>      // uint32_t
#include <QtCore>
#include <glass_types.h>

// ============================================================================
// Definition
// ============================================================================

class vm_render_t : public QObject {
    Q_OBJECT

signals:

    void dirty_rect(uint32_t, glass_rect_t);
};

#endif // VGLASS_HACK_H
