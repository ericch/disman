#ifndef DEDICATEDRESOURCE_H
#define DEDICATEDRESOURCE_H

//
// Dedicated Resource
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <memory>       // std::unique_ptr
#include <cstdint>      // int32_t, uint32_t
#include <QtCore>

extern "C" {
    #include <pv_display_backend_helper.h>
};

// ============================================================================
// Macros / Definitions
// ============================================================================

const uint32_t DISCRETE_MASK = 0x80000000;

// ============================================================================
// DedicatedResource Definition
// ============================================================================

class DedicatedResource : public QObject
{
    Q_OBJECT

    using domid_t = uint16_t;
    using uuid_t = QUuid;

public:

    DedicatedResource(const uuid_t uuid, const domid_t domid);
    virtual ~DedicatedResource();

    static void pv_advertise_list_request(struct pv_display_consumer *consumer,
                                          struct dh_display_advertised_list *advertised_list);
    static void pv_driver_fatal_consumer_error(struct pv_display_consumer *consumer);
    static void new_control_channel_connection(void *opaque, struct libivc_client *client);

signals:

    void displays_changed(const uuid_t& uuid, const QList<dh_display_info>& display_info, const uint32_t size);

private slots:

    void init();
    void destroy();

    void add_display(uint32_t key, uint32_t x, uint32_t y, uint32_t width, uint32_t height);
    void remove_old_displays();
    void displaysChanged();
    void handle_error();
    void finish_control_connection(void *cli);

private:

    uuid_t m_uuid;
    domid_t m_domid;
    uint32_t m_next_pv_port;
    struct pv_display_consumer *m_pv_backend_consumer;
    QMutex m_lock;

    QMap<uint32_t, dh_display_info> m_display_info_internal;
    QSet<uint32_t> m_newkeys;
};

#endif // DEDICATEDRESOURCE_H
